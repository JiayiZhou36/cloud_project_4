use actix_files::Files;
use actix_web::{web, App, HttpServer, Responder};
use rand::Rng;

async fn generate_random_number() -> impl Responder {
    // Generate a random number between 1 and 100
    let mut rng = rand::thread_rng();
    let random_number: u32 = rng.gen_range(1..=100);
    format!("Random Number: {}", random_number)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/random-number", web::get().to(generate_random_number))
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind(("0.0.0.0", 50505))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use actix_web::{dev::Service, http, test, web, App, Error};

    use super::*;

    #[actix_web::test]
    async fn test_index() -> Result<(), Error> {
        let app = App::new().route("/random-number", web::get().to(generate_random_number));
        let app = test::init_service(app).await;

        let req = test::TestRequest::get().uri("/random-number").to_request();
        let resp = app.call(req).await?;

        assert_eq!(resp.status(), http::StatusCode::OK);
        Ok(())
    }
}
