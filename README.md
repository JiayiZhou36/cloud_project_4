# Cloud_Project_4
By Jiayi Zhou
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud_project_4/badges/master/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud_project_4/-/commits/master)

## Purpose of Project
This project containerizes a Rust Actix Web Service--random number generator.

## Requirements
* Containerize simple Rust Actix web app
* Build Docker image
* Run container locally

## Website
![Screenshot_2024-02-18_at_5.53.05_PM](/uploads/2110496653d0860d27f631266d6b05a2/Screenshot_2024-02-18_at_5.53.05_PM.png)
![Screenshot_2024-02-18_at_5.53.10_PM](/uploads/8ed0f392acbe4f358275018c4cf38581/Screenshot_2024-02-18_at_5.53.10_PM.png)

## Container Image
![Screenshot_2024-02-18_at_5.32.55_PM](/uploads/e161c65d1344fbfc3b010db5dc91a0a9/Screenshot_2024-02-18_at_5.32.55_PM.png)

## Container
![Screenshot_2024-02-18_at_5.33.19_PM](/uploads/8d69a1ffd597b1f39be0c6698e6a9e00/Screenshot_2024-02-18_at_5.33.19_PM.png)

## Reference
1. https://actix.rs/docs/getting-started